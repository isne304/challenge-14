#include<iostream>
#include <algorithm>
using namespace std;
void quicksort(int a[], int size) {
	int ar1 = a[0];
	int ar2 = a[size / 2];
	int ar3 = a[size - 1];
	int med[] = { ar1,ar2,ar3 };
	int s = sizeof(med) / sizeof(int);
	sort(med, med + s);
	int median = med[1];
	int size1 = 0, size2 = 0;
	for (int i = 0; i < size; i++) {
		if (a[i] < median)
			size1++;
		else if (a[i] > median)
			size2++;
	}
	int *sa1 = new int[size1];
	int *sa2 = new int[size2];
	if (size == 2) {
		if (a[0] > a[1]) {
			swap(a[0], a[1]);
		}
	}
	if (size >= 3) {
		int s1 = 0, s2 = 0;
		for (int i = 0; i < size; i++) {
			if (a[i] < median) {
				sa1[s1] = a[i];
				s1++;
			}
			else if (a[i] > median) {
				sa2[s2] = a[i];
				s2++;
			}
		}
		quicksort(sa1, size1);
		quicksort(sa2, size2);
		for (int j = 0; j < size1; j++) {
			a[j] = sa1[j];
		}
		a[size1] = median;
		for (int j = 0; j < size2; j++) {
			a[size1 + 1 + j] = sa2[j];
		}
	}
}

void mergesort(int a[], int p, int size) {
	int q;
	if (p<size) {
		q = (p + size) / 2;
		mergesort(a, p, q);
		mergesort(a, q + 1, size);
		int n1 = q - p + 1;
		int n2 = size - q;
		int l[n1 + 1];
		int r[n2 + 1];
		for (int i = 1; i <= n1; i++) {
			l[i] = a[p + i - 1];
		}
		for (int j = 1; j <= n2; j++) {
			r[j] = a[q + j];
		}
		int i = 1, j = 1;
		l[n1 + 1] = 999;
		r[n2 + 1] = 999;
		for (int k = p; k <= size; k++) {
			if (l[i] <= r[j]) {
				a[k] = l[i];
				i = i + 1;
			}
			else {
				a[k] = r[j];
				j = j + 1;
			}
		}
	}
}
int main() {
	int a[10] = { 5,6,9,4,8,7,2,1,3,10 };
	cout << "quick sort" << endl;
	cout << "sorted form" << endl;
	quicksort(a, 10);
	for (int i = 0; i < 10; i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;

	int b[10] = { 5,6,9,4,8,7,2,1,3,10 };
	cout << "marge sort" << endl;
	cout << "sorted form" << endl;
	mergesort(b, 1, 10);
	for (int i = 0; i < 10; i++)
	{
		cout << b[i] << " ";
	}
	cout << endl;

	return 0;
}

